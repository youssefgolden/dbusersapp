import os
from flask import Flask, redirect, render_template, url_for
from flask_sqlalchemy import SQLAlchemy

basedir=os.path.abspath(os.path.dirname(__file__))

app=Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///'+os.path.join(basedir,'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False

db=SQLAlchemy(app)

class Role(db.Model):
    __tablename__="roles"
    id= db.Column(db.Integer,primary_key=True)
    name= db.Column(db.String(32), unique=True)
    users=db.relationship("User",backref="role",lazy=True)

class User(db.Model):
    __tablename__="users"
    id= db.Column(db.Integer,primary_key=True)
    username= db.Column(db.String(32),unique=True,index=True)
    password= db.Column(db.String(32), nullable=False)
    role_id=db.Column(db.Integer, db.ForeignKey("roles.id"))

@app.route("/")
def index():
    data=Role.query.all()
    return render_template("index.html",d=data)

@app.route("/add/<user>")
def add(user):
    us=Role(name=user)
    db.session.add(us)
    db.session.commit()
    return redirect(url_for("index"))

if __name__ == "__main__":
    app.run(debug=True)